package com.authback.SecurityConfiguration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * configuration of spring security to depend on a user service (userDetailService)
 */

//we add this annotation to tell spring security this is a configuration class
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired
     UserDetailsService userDetailsService;

     //use authentificationManagerBuilder to configure authentification
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //spring security will call the method on the user detailed service to get user information
        // every time there is an authentificattion attempt
        auth.userDetailsService(userDetailsService);

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //set up different authorization for different API (admin , chefProjet, dev,testeur)
        http.authorizeHttpRequests()
                .antMatchers("/admin").hasAnyAuthority("ADMIN")
                .antMatchers("/user").hasAnyAuthority("ADMIN,USER")
                .antMatchers("/").permitAll()
                .and().formLogin();
    }

    //create password encoder because spring security expect to have one
    //in this methode we didn't spcify the encoder , just we want clear text
    //it's for test and because our password is encoded in client part
    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return NoOpPasswordEncoder.getInstance();
    }


}
