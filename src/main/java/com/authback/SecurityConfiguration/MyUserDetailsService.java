package com.authback.SecurityConfiguration;

import com.authback.models.User;
import com.authback.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MyUserDetailsService  implements UserDetailsService {


    private final UserRepository userRepository;

    @Autowired
    public MyUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        System.out.println("zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz  : : "+userName);
        Optional<User> user = this.userRepository.findByUserName(userName);
        user.orElseThrow(()->new UsernameNotFoundException("Not found ++++++: "+userName));
        System.out.println(user.map(MyUserDetails::new).get());
        return user.map(MyUserDetails::new).get();
    }
}
