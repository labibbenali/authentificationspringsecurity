package com.authback;

import com.authback.repository.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = UserRepository.class)
public class TokenBAckApplication {

    public static void main(String[] args) {
        SpringApplication.run(TokenBAckApplication.class, args);
    }

}
